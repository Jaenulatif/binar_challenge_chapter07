// import logo from './logo.svg';
// import './App.css';
import{
  BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom';

import AllCar from './Components/Pages/All-Car';
import Sewa from './Components/Pages/Rent-Car';


function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<AllCar />}/>
          <Route path="/sewa" element={<Sewa />}/>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
