import Banner from "../../Shared/Banner";
import Navbar from "../../Shared/Navbar";
import "../../../Assets/Styles/Rent-Car.css";
import Footer from "../../Shared/Footer";
import axios from "axios";
import { useEffect, useState } from "react";
import moment from 'moment';

const Sewa = () => {

    // This state
    const [cars, setCars] = useState([]);
    const [filterCar, setFilterCar] = useState({
        driver: '',
        tanggal: '',
        waktu: '',
        penumpang: 0

    })

    useEffect(() => {
        axios.get('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json')
            .then(response => {
                setCars(response.data);
            }).catch(err => {
                console.error(err);
            });
    }, []);

    // const getDriverType = (e) => {
    //     setFilterCar({
    //         driver: e.target.value
    //     });
    // }

    const getDateRent = (e) => {
        setFilterCar((prevState) => ({
            ...prevState,
            tanggal: e.target.value
        }))
    }

    const getTimeRent = (e) => {
        setFilterCar((prevState) => ({
            ...prevState,
            penumpang: parseInt(e.target.value)
        }));
    }

    const getPassengerCount = (e) => {
        setFilterCar((prevState) => ({
            ...prevState,
            penumpang: parseInt(e.target.value)
        }));
    }
    

    const doFilterCars = () => {
        const filteredCarsData = cars.filter((item)=> item.capacity === filterCar.penumpang && moment(item.availableAt).format("L") == moment(filterCar.tanggal).format("L"));
        
        console.info(filteredCarsData);

        setCars(filteredCarsData);

        cars.forEach(val => {
            console.info("PARSED DATE TIME : ", moment(val.avaialbleAt).format("L"));
            console.info("PARSED DATE FILTER PARAM : ", moment(filterCar.tanggal).format("L"));
        })
    }

    return(
        <>
            <Navbar />
            <Banner />
            <section id="filter" className="container" >
                <form className="row" id="form">
                    <div className="col-lg-2 offset-lg-1">
                        <label for="">Driver</label>
                        <div className="dropdown" >
                        <button className="btn dropdown-toggle" type="button" id="btn-sopir" data-bs-toggle="dropdown" aria-expanded="false" style={{width: "100%"}}>
                            Pilih Tipe Driver
                        </button>
                        <ul className="dropdown-menu dropdown-menu-sopir" id="sopir" aria-labelledby="dropdownMenuButton1">
                            <li><a className="dropdown-item">Dengan Sopir</a></li>
                            <li><a className="dropdown-item">Tanpa Sopir</a></li>
                        </ul>
                        </div>
                    </div>
                    <div className="col-lg-2">
                        <label for="tanggal-pinjam">Tanggal</label>
                        <input type="date" className="form-control" id="tanggal-pinjam" style={{width: "100%"}} onChange={event => getDateRent(event)} />
                    </div>
                    <div className="col-lg-2">
                        <label for="waktu-jemput">Waktu Jemput/Ambil</label>
                        <div className="dropdown" id="">
                        <button className="btn dropdown-toggle" type="button" id="waktu-jemput" data-bs-toggle="dropdown" aria-expanded="false" style={{width: "100%"}}>
                            Pilih Waktu
                        </button>
                        <ul className="dropdown-menu dropdown-menu-jam" aria-labelledby="dropdownMenuButton1">
                            <li><a className="dropdown-item">08.00 WIB</a></li>
                            <li><a className="dropdown-item">09.00 WIB</a></li>
                            <li><a className="dropdown-item">10.00 WIB</a></li>
                            <li><a className="dropdown-item">11.00 WIB</a></li>
                            <li><a className="dropdown-item">12.00 WIB</a></li>
                        </ul>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <label for="penumpang">Jumlah Penumpang(Optional)</label>
                        <input type="number" id="penumpang" onChange={event => getPassengerCount(event)} className="form-control" id="autoSizingInputGroup" placeholder="Jumlah Penumpang" style={{width: "100%"}} />
                    </div>
                    <div className="col-lg-1">
                        <button id="submit" className="btn btn-success" onClick={() => doFilterCars()} >Cari Mobil</button>
                    </div>
                </form>
            </section>
            <section id="content" className="container">
                <div className="cars-container row">
                    {cars.map((car, index) => {
                        return<>
                        <div className="col-md-4 my-2" key={car.id}>
                            <div className="card h-100">
                                <div className="card-body">
                                <img src={car.image} className="card-img" style={{}} alt="img-car"/>
                                    <h6 className="card-title mt-3 card-title-text">
                                        {car.manufacture}/{car.model}
                                    </h6>
                                    <h5 className="card-title mt-3 card-title-text">Rp {car.rentPerDay} / hari</h5>
                                    <p className="card-text">{car.description}</p>
                                    <h6>
                                        <i className="bi bi-people me-2" style={{ fontSize: 20 + "px" }}></i> {car.capacity} orang
                                    </h6>
                                    <h6>
                                        <i className="bi bi-gear me-2" style={{ fontSize: 20 + "px" }}></i> {car.transmission}
                                    </h6>
                                    <h6>
                                        <i className="bi bi-calendar me-2" style={{ fontSize: 20 + "px" }}></i> Tahun {car.year}
                                    </h6>
                                    <div className="d-flex flex-column mt-3 align-items-stretch">
                                    <button className="btn btn-success">Pilih Mobil</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </>
                    })}
                </div>
                
            </section>

            <Footer />
        </>
    )
}

export default Sewa;