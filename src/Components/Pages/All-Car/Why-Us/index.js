import "../../../../Assets/Styles/Why-Us.css";
import icon_complate from "../../../../Assets/Images/icon_complete.svg";
import icon_price from "../../../../Assets/Images/icon_price.svg";
import icon_24hrs from "../../../../Assets/Images/icon_24hrs.svg";
import icon_professional from "../../../../Assets/Images/icon_professional.svg";

const WhyUs = () => {
    return (
        <>
            <section id="why-us" class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2><strong>Why Us</strong></h2>
                            <p>Mengapa harus pilih Binar Car Rental?</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 mt-3">
                            <div class="card" style={{height: "200px"}}>
                                <img src={icon_complate} class="card-img-top icn-card" alt="" />
                                <div class="card-body">
                                    <h5 class="card-title">Mobil Lengkap</h5>
                                    <p class="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 mt-3">
                            <div class="card" style={{height: "200px"}}>
                                <img src={icon_price} class="card-img-top icn-card" alt="" />
                                <div class="card-body">
                                    <h5 class="card-title">Harga Murah</h5>
                                    <p class="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 mt-3">
                            <div class="card" style={{height: "200px"}}>
                                <img src={icon_24hrs} class="card-img-top icn-card" alt="" />
                                <div class="card-body">
                                    <h5 class="card-title">Layanan 24 Jam</h5>
                                    <p class="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 mt-3">
                            <div class="card" style={{height: "200px"}}>
                                <img src={icon_professional} class="card-img-top icn-card" alt="" />
                                <div class="card-body">
                                    <h5 class="card-title">Sopir Profesional</h5>
                                    <p class="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default WhyUs;