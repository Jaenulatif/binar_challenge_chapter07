import img_person1 from "../../../../Assets/Images/person-1.svg";
import img_person2 from "../../../../Assets/Images/person-2.svg";
import img_rate from "../../../../Assets/Images/rate.png";
import "../../../../Assets/Styles/Testimonial.css";

import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

const Testimonial = () => {
    return(
        <>
            <section id="testimonial" className="container">
                <div className="row">
                    <div className="col-12">
                        <h2 className="testimonial-title"><strong>Testimonial</strong></h2>
                        <p className="testimonial-title">Berbagai review positif dari para pelanggan kami</p>
                    </div>
                </div>
                

                <OwlCarousel className="owl-carousel owl-theme">
                    <div className="item row-4">
                        <div className="card mb-3" style={{maxWidth: "540px"}}>
                            <div className="row g-0">
                                <div className="col-lg-3">
                                    <img src={img_person1} className="img-fluid rounded-start person" alt="..." />
                                </div>
                                <div className="col-lg-9">
                                    <div className="card-body">
                                    <img src={img_rate} alt="" className="rate"/>
                                    <p className="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                    <h5 className="card-title"><strong>John Dee 32, Bromo</strong></h5>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="item row-4">
                        <div className="card mb-3" style={{maxWidth: "540px"}}>
                            <div className="row g-0">
                                <div className="col-lg-3">
                                    <img src={img_person2} className="img-fluid rounded-start person" alt="..." />
                                </div>
                                <div className="col-lg-9">
                                    <div className="card-body">
                                    <img src={img_rate} alt="" className="rate"/>
                                    <p className="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                    <h5 className="card-title"><strong>John Dee 32, Bromo</strong></h5>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="item row-4">
                        <div className="card mb-3" style={{maxWidth: "540px"}}>
                            <div className="row g-0">
                                <div className="col-lg-3">
                                    <img src={img_person1} className="img-fluid rounded-start person" alt="..." />
                                </div>
                                <div className="col-lg-9">
                                    <div className="card-body">
                                        <img src={img_rate} alt="" className="rate"/>
                                    <p className="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                    <h5 className="card-title"><strong>John Dee 32, Bromo</strong></h5>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="item row-4">
                        <div className="card mb-3" style={{maxWidth: "540px"}}>
                            <div className="row g-0">
                                <div className="col-lg-3">
                                    <img src={img_person2} className="img-fluid rounded-start person" alt="..." />
                                </div>
                                <div className="col-lg-9">
                                    <div className="card-body">
                                        <img src={img_rate} alt="" className="rate"/>
                                    <p className="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                    <h5 className="card-title"><strong>John Dee 32, Bromo</strong></h5>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="item row-4">
                        <div className="card mb-3" style={{maxWidth: "540px"}}>
                            <div className="row g-0">
                                <div className="col-lg-3">
                                    <img src={img_person2} className="img-fluid rounded-start person" alt="..." />
                                </div>
                                <div className="col-lg-9">
                                    <div className="card-body">
                                        <img src={img_rate} alt="" className="rate" />
                                    <p className="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                    <h5 className="card-title"><strong>John Dee 32, Bromo</strong></h5>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ OwlCarousel>
            </section>
        </>
    )
}


export default Testimonial;