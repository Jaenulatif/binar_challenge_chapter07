import check_list from "../../../../Assets/Images/checkList.svg";
import img_services from "../../../../Assets/Images/img_service.svg";

const OurServices = () => {
    return (
        <>
            <section id="our-service" class="container-fluid my-5">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <img src={img_services} class="img-fluid" alt="banner kedua cewe" />
                        </div>
                        <div class="col-lg-6 mt-5">
                            <h2><strong>Best Car Rental for any kind of trip in (Lokasimu)!</strong></h2>
                            <p class="mt-4">Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, 
                                kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
                            </p>
                            <div id="desk-list">
                                <p><img src={check_list} alt="check-list" />  sewa Mobil Dengan Supir di Bali 12 Jam</p>
                                <p><img src={check_list} alt="check-list" />  Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
                                <p><img src={check_list} alt="check-list" />  Sewa Mobil Jangka Panjang Bulanan</p>
                                <p><img src={check_list} alt="check-list" />  Gratis Antar - Jemput Mobil di Bandara</p>
                                <p><img src={check_list} alt="check-list" />  Layanan Airport Transfer / Drop In Out</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default OurServices;