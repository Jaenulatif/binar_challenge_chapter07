import Navbar from "../../Shared/Navbar";
import Banner from "../../Shared/Banner";
import OurServices from "./Our-Services";
import WhyUs from "./Why-Us";
import Testimonial from "./Testimonial";
import Sewa from "./Sewa";
import Faq from "./FAQ";
import Footer from "../../Shared/Footer"

const AllCar = () => {
    return(
        <>
            <Navbar />
            <Banner />
            <OurServices />
            <WhyUs />
            <Testimonial />
            <Sewa />
            <Faq />
            <Footer />
        </>
    )
}

export default AllCar;