import "../../../../Assets/Styles/Sewa.css"

const Sewa = () => {
    return(
        <>
            <section id="sewa" className="container-fluid">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-10 offset-lg-1">
                            <div className="card text-center">
                                <div className="card-body">
                                    <h1 className="card-title"><strong>Sewa Mobil di (Lokasimu) Sekarang</strong></h1>
                                    <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                    <button href="#" className="btn btn-primary">Mulai Sewa Mobil</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Sewa;