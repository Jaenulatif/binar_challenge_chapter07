import icn_fb from "../../../Assets/Images/icon_facebook.svg";
import icn_ig from "../../../Assets/Images/icon_instagram.svg";
import icn_twt from "../../../Assets/Images/icon_twitter.svg";
import icn_mail from "../../../Assets/Images/icon_mail.svg";
import icn_twitch from "../../../Assets/Images/icon_twitch.svg";

import "../../../Assets/Styles/Footer.css";


const Footer = () => {
    return(
        <>
            <section id="footer" class="container-fluid" >
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4">
                            <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                            <p >binarcarrental@gmail.com</p>
                            <p>081-233-334-808</p>
                        </div>
                        <div class="col-lg-2">
                            <p><a href="#our-service"><strong>Our services</strong></a></p>
                            <p><a href="#why-us"><strong>Why Us</strong></a></p>
                            <p><a href="#testimonial"><strong>Testimonial</strong></a></p>
                            <p><a href="#faq"><strong>FAQ</strong></a></p>
                        </div>
                        <div class="col-lg-3">
                            <p>Connect with us</p>
                            <p><img alt="images_icon" src={icn_fb}  /> <img alt="images_icon" src={icn_ig} /><img alt="images_icon" src={icn_twt} /><img alt="images_icon" src={icn_mail} /><img alt="images_icon" src={icn_twitch} /></p>
                        </div>
                        <div class="col-lg-2">
                            <p>Copyright Binar 2022</p>
                            <p><div id="logo"></div></p>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Footer;