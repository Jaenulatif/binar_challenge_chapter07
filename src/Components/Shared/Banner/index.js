import "../../../Assets/Styles/Banner.css";
import img_car from "../../../Assets/Images/img_car.svg";

const Banner = () => {
    return (
        <>
            <section id="banner" className="container-fluid">
                <div className="row">
                    <div className="col-lg-5 offset-lg-1 banner-content-left mt-lg-5 col-sm-12 offset-sm-0">
                        <h1 id=""><strong>Sewa & Rental Mobil Terbaik di Kawasan (Lokasimu)</strong></h1>
                        <p className="mt-2">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                        <button type="button" className="btn btn-primary">Mau Sewa Mobil</button>
                    </div>
                    <div className="col-lg-6 mt-5">
                        <img src={img_car} alt="banner mobil" />
                    </div>
                </div>
            </section>
        </>
    )
}

export default Banner;