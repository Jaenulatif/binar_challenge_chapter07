import "../../../Assets/Styles/Navbar.css";

const Navbar = () => {
    return (
        <>
            <section id="navigation">
                <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                    <div class="container-fluid">
                        <button class="navbar-brand" href="#"><div id="logo"></div></button>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ms-auto mb-2 mb-lg-0" >
                                <li class="nav-item me-3">
                                    <a class="nav-link" aria-current="page" href="#our-service">Our Services</a>
                                </li>
                                <li class="nav-item me-3">
                                    <a class="nav-link" href="#why-us">Why Us</a>
                                </li>
                                <li class="nav-item me-3">
                                    <a class="nav-link" href="#testimonial">Testimonial</a>
                                </li>
                                <li class="nav-item me-3">
                                    <a class="nav-link" href="#faq">FAQ</a>
                                </li>
                            </ul>
                            <form class="d-flex">
                            <button class="btn btn-outline-success me-3" type="submit">Register</button>
                            </form>
                        </div>
                    </div>
                </nav>
            </section>
        </>
    )
}

export default Navbar;